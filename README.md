# Swooper

This is a simple Minesweeper implementation for Plasma Mobile!

## Dependencies to build
- extra-cmake-modules
- ki18n
- kirigami2
- qt5
- qt5-qtquickcontrols2
