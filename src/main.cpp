#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>

#include "game.h"
#include "scores.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{    
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("Swooper");

    QQmlApplicationEngine engine;
    
    auto *gameObject = new GameObject();
    engine.rootContext()->setContextProperty("gameObject", gameObject);
    auto *scoreModel = new ScoreModel();
    engine.rootContext()->setContextProperty("scoreModel", scoreModel);
    
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
