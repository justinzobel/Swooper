#include "game.h"

GameObject::GameObject(QObject *parent) : QObject(parent) {
    this->newGame(8,8,10);
}

void GameObject::registerClicked(int index){
    if (this->flagMode){
        this->flagged(index);
    }else{
        this->uncovered(index);
    }
}

void GameObject::registerHeld(int index){
    if (this->flagMode){
        this->uncovered(index);
    }else{
        this->flagged(index);
    }
}

void GameObject::flagged(int index){
    if (this->board->fields[index].isCovered){
        this->board->flag(index);
    }else{
        this->board->uncover(index);
    }
    this->updateImages();
}

void GameObject::uncovered(int index){
    this->board->uncover(index);
    this->updateImages();
}

void GameObject::updateImages(){
    if (!this->isStarted){
        this->isStarted = true;
        this->startTime = QDateTime::currentDateTime();
        emit gameStarted();
    }
    
    if (this->board->isLost){
        this->isLost=true;
        this->statusImage="qrc:///status_lost.png";
        emit gameLost();
    }else if (this->board->isWon){
        this->isWon=true;
        this->statusImage="qrc:///status_won.png";
        emit gameWon();
    }
    
    QStringList images=QStringList();
    for (int i=0; i<nElements; i++){
        if(this->isLost && this->board->fields[i].isBomb){
            images.append("qrc:///bomb.png");
        }else if (this->board->fields[i].isCovered && !this->board->fields[i].isFlag){
            images.append("qrc:///cover.png");
        }else if(!this->board->fields[i].isCovered && !this->board->fields[i].isBomb){
            switch(this->board->fields[i].nnBombs){
                case 1:
                    images.append("qrc:///1.png");
                    break;
                case 2:
                    images.append("qrc:///2.png");
                    break;
                case 3:
                    images.append("qrc:///3.png");
                    break;
                case 4:
                    images.append("qrc:///4.png");
                    break;
                case 5:
                    images.append("qrc:///5.png");
                    break;
                case 6:
                    images.append("qrc:///6.png");
                    break;
                case 7:
                    images.append("qrc:///7.png");
                    break;
                case 8:
                    images.append("qrc:///8.png");
                    break;
                default:
                    images.append("qrc:///cleared.png");
            }
        }else if(this->board->fields[i].isFlag){
            images.append("qrc:///flag.png");
        }else if(!this->board->fields[i].isCovered && this->board->fields[i].isBomb){
            images.append("qrc:///bomb.png");
        }
    }
    
    this->images = images;
    emit updated();
}

void GameObject::toggleFlagMode(){
    this->flagMode = (this->flagMode) ? false : true;
    emit updated();
}

void GameObject::getTimer(){
    if (!isLost && !isWon && isStarted){
        this->timer = floor(startTime.msecsTo(QDateTime::currentDateTime())/1000);
        emit timerUpdated();
    }
}

void GameObject::newGame(int new_cols, int new_rows, int new_nBombs){
    this->columns=new_cols;
    this->rows=new_rows;
    this->nElements=new_cols*new_rows;
    this->nBombs=new_nBombs;
    
    this->flagMode=false;
    
    this->isWon=false;
    this->isLost=false;
    this->isStarted=false;
    
    if (this->columns==8 && this->rows==8 && this->nBombs==10){
        this->difficulty = "Easy";
    }else if (this->columns==16 && this->rows==16 && this->nBombs==40){
        this->difficulty = "Normal";
    }else if (this->columns==16 && this->rows==30 && this->nBombs==99){
        this->difficulty = "Hard";
    }else{
        this->difficulty = "Custom";
    }
                    
    this->statusImage="qrc:///status_normal.png";
    
    this->timer = 0;
    
    this->images=QStringList();
    for (int i=0; i<nElements; i++){
        this->images.append("qrc:///cover.png");
    }
    
    this->board = new GameBoard(new_cols, new_rows, new_nBombs);
    
    emit gameLoaded();
    emit timerUpdated();
    emit updated();
}

int GameObject::getToReveal(){
    return this->board->toReveal;
}

int GameObject::getNFlags(){
    return this->board->nFlags;
}
