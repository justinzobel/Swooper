set(swooper_SRCS
    main.cpp
    board.cpp
    game.cpp
    field.cpp
    scores.cpp
    )
    
set(swooper_HDRS
    board.h
    game.h
    field.h
    scores.h
    )

qt6_add_resources(RESOURCES qml.qrc)

add_executable(Swooper ${swooper_SRCS} ${swooper_HDRS} ${RESOURCES})

find_package(Qt6 REQUIRED COMPONENTS Gui Qml QuickControls2 Widgets)

find_package(KF6Kirigami2)
find_package(KF6I18n)
find_package(KF6CoreAddons)
find_package(KF6Config)

target_link_libraries(Swooper  Qt6::Core Qt6::Quick Qt6::Qml Qt6::Gui Qt6::QuickControls2 Qt6::Widgets)
target_link_libraries(Swooper KF6::Kirigami2)
target_link_libraries(Swooper KF6::I18n KF6::ConfigCore KF6::CoreAddons)

install(TARGETS Swooper ${KF6_INSTALL_TARGETS_DEFAULT_ARGS})
