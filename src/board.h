#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <stdlib.h>  // GameBoard for rand()
#include <math.h>  // GameBoard for floor() 
#include <ctime> // GameBoard for time()
#include "field.h"

class GameBoard {
public:
    GameBoard(int c, int r, float pb);
    ~GameBoard();
    
    int columns;
    int rows;
    int nElements;
    int nBombs;
    int nFlags;
    int toReveal;

    bool isLost;
    bool isWon;
    
    Field *fields;
    
    void flag(int index);
    void uncover(int index);

private:
    void setNnBombs(); // called once at the start to set nbombs for all fields
    int getCol(int index);
    int getRow(int index);
    int getIndex(int column, int row);
    bool isValid(int column, int row);
};

#endif // GAMEBOARD_H
