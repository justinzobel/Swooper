import QtQuick 2.6
import QtQuick.Controls 2.1
import org.kde.kirigami 2.11 as Kirigami

Kirigami.ApplicationWindow
{
    id: appwindow

    function switchToPage(pagename) {
        var page;
        if(pagename == "board") page = boardpage;
        else if(pagename == "settings") page = settingspage;
        else if(pagename == "highscores") page = scorepage;
        else return;

        if(pageStack.depth > 1)
            pageStack.pop()
        if(page != boardpage)
            pageStack.push(page)
    }

    globalDrawer: Kirigami.GlobalDrawer {
        id: globalDrawer
        showHeaderWhenCollapsed: true
        header: ToolButton {
                icon.name: "application-menu"
                visible: globalDrawer.collapsible
                checked: !globalDrawer.collapsed
                onClicked: globalDrawer.collapsed = !globalDrawer.collapsed
            }

        actions: [
            Kirigami.Action {
                text: "Board"
                onTriggered: switchToPage("board")
            },
            Kirigami.Action {
                text: "Settings"
                onTriggered: switchToPage("settings")
            },
            Kirigami.Action {
                text: "Highscores"
                onTriggered: switchToPage("highscores")
            }
        ]
    }
    pageStack.initialPage: boardpage

    Popup {
        id: highscoreDialog
        parent: applicationWindow().overlay
        anchors.centerIn: parent
        Column {
            TextField {
                id: userInput
                maximumLength:20
                placeholderText: scoreModel.lastUser
                validator: RegExpValidator { regExp: /[a-zA-Z0-9 ]+/ }
            }
            Row{
                Button {
                    text: "Ok"
                    onClicked:{
                        scoreModel.addScore(userInput.text,gameObject.difficulty,gameObject.timer)
                        scoreModel.saveScores()
                        highscoreDialog.close()
                    }
                }
                Button {
                    text: "Cancel"
                    onClicked: highscoreDialog.close()
                }
            }
        }
    }
    
    // ************* Pages **************//
    GameBoardPage {
        id: boardpage
        objectName: "board"
    }

    SettingsPage {
        id: settingspage
        objectName: "settings"
        visible: false
    }
    
    HighScorePage {
        id: scorepage
        objectName: "highscores"
        visible: false
    }
}
