import QtQuick 2.12
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.1 as Controls
import QtQuick.Layouts 1.2


Kirigami.ScrollablePage {
    id:highscores
    title: "Highscores"
    implicitWidth: applicationWindow().width
    Kirigami.CardsListView {
        model: scoreModel
        anchors.fill: parent
        delegate: Kirigami.AbstractCard{
            contentItem: Item{
                implicitWidth: delegateLayout.implicitWidth
                implicitHeight: delegateLayout.implicitHeight
                RowLayout{
                id:delegateLayout
                Image{ source: model.image }
                Text{ text: model.type; font.bold: true }
                Kirigami.Separator{}
                Text{ text: (model.time<1000) ? model.name + " - " + model.time + "s" : "No highscore"  }
                }
            }
        }
    }
}
